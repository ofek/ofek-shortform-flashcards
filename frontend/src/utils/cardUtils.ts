import Card from '@/types/Card';

const msUntilShown = (card: Card, binWaitTimeMs: number): number => {
  return Math.max(
    0,
    binWaitTimeMs - (Date.now() - (card.lastReviewed?.getTime() ?? 0)),
  );
};

export { msUntilShown };
