import { v4 as uuidv4 } from 'uuid';

import Bin from '@/types/Bin';

const generateDefaultBins = (): { [name: string]: Bin } =>
  [
    {
      name: 'initial',
      waitTimeMs: 0,
      order: 0,
    },
    {
      name: '5s',
      waitTimeMs: 5 * 1000,
      order: 1,
    },
    {
      name: '25s',
      waitTimeMs: 25 * 1000,
      order: 2,
    },
    {
      name: '2m',
      waitTimeMs: 2 * 60 * 1000,
      order: 3,
    },
    {
      name: '10m',
      waitTimeMs: 10 * 60 * 1000,
      order: 4,
    },
    {
      name: '1h',
      waitTimeMs: 1 * 60 * 60 * 1000,
      order: 5,
    },
    {
      name: '5h',
      waitTimeMs: 5 * 60 * 60 * 1000,
      order: 6,
    },
    {
      name: '1d',
      waitTimeMs: 1 * 24 * 60 * 60 * 1000,
      order: 7,
    },
    {
      name: '5d',
      waitTimeMs: 5 * 24 * 60 * 60 * 1000,
      order: 8,
    },
    {
      name: '25d',
      waitTimeMs: 25 * 24 * 60 * 60 * 1000,
      order: 9,
    },
    {
      name: '4mo',
      waitTimeMs: 4 * 30 * 24 * 60 * 60 * 1000,
      order: 10,
    },
    {
      name: 'never',
      neverShow: true,
      order: 11,
    },
    {
      name: 'hardToRemember',
      neverShow: true,
    },
  ].reduce((dict: { [name: string]: Bin }, currBin: Partial<Bin>) => {
    currBin.id = uuidv4();
    currBin.cards = [];

    dict[currBin.id] = currBin as Bin;
    return dict;
  }, {});

export { generateDefaultBins };
