import { InjectionKey } from 'vue';
import { createStore, useStore as baseUseStore, Store } from 'vuex';
import { v4 as uuidv4 } from 'uuid';

import Bin from '@/types/Bin';
import Card from '@/types/Card';
import { generateDefaultBins } from '@/utils/binUtils';
import { msUntilShown } from '@/utils/cardUtils';

interface State {
  bins: {
    [name: string]: Bin;
  };
}

const store = createStore<State>({
  state: {
    bins: generateDefaultBins(),
  },
  getters: {
    getSortedBins(state) {
      return Object.values(state.bins)
        .filter((b: Bin) => typeof b.order === 'number')
        .sort((bin1, bin2) => (bin1.order as number) - (bin2.order as number));
    },
    getHardBin(state) {
      return Object.values(state.bins).find(
        (b: Bin) => b.name === 'hardToRemember',
      );
    },
    getReviewableBins(state, getters) {
      return getters.getSortedBins.filter(
        (b: Bin) => !b.neverShow && typeof b.waitTimeMs === 'number',
      );
    },
    getReviewList(state, getters) {
      const zeroTimeCards = getters.getReviewableBins
        .flatMap((b: Bin) =>
          b.cards
            .filter((c: Card) => msUntilShown(c, b.waitTimeMs as number) === 0)
            .map((c: Card) => ({
              card: c,
              binId: b.id,
            })),
        )
        .reverse();

      return zeroTimeCards.concat(getters.getSortedBins[0]?.cards ?? []);
    },
    getIsPermanentlyDone(state, getters) {
      return (
        getters.getReviewableBins.flatMap((b: Bin) => b.cards).length === 0
      );
    },
  },
  mutations: {
    addCard(state, { card, binId }) {
      card.id = uuidv4();
      state.bins[binId].cards.push(card);
    },
    moveCard(state, { cardId, fromBinId, toBinId }) {
      const fromBin = state.bins[fromBinId];
      const toBin = state.bins[toBinId];

      const removalIndex = fromBin.cards.findIndex(
        (c: Card) => c.id === cardId,
      );

      if (removalIndex < 0 || !fromBin || !toBin) {
        return;
      }

      const card = fromBin.cards[removalIndex];

      fromBin.cards.splice(removalIndex, 1);

      toBin.cards.push(card);
    },
    updateCard(state, { cardId, binId, newCard }) {
      const bin = state.bins[binId];

      const oldCardIndex = bin.cards.findIndex((c: Card) => c.id === cardId);

      const oldCard = bin.cards[oldCardIndex];

      state.bins[binId].cards[oldCardIndex] = {
        ...oldCard,
        ...newCard,
        id: cardId,
      };
    },
    updateLastReviewed(state, { cardId, binId }) {
      const card = state.bins[binId].cards.find((c: Card) => c.id === cardId);

      if (!card) {
        return;
      }

      card.lastReviewed = new Date();
    },
    incrementIncorrectCount(state, { cardId, binId }) {
      const card = state.bins[binId].cards.find((c: Card) => c.id === cardId);

      if (!card) {
        return;
      }

      card.incorrectCount = card.incorrectCount + 1;
    },
  },
  actions: {
    registerRightAnswer({ state, commit, getters }, { cardId, binId }) {
      const currBin = state.bins[binId];
      const card = currBin?.cards.find((c: Card) => c.id === cardId);

      if (!card) {
        return;
      }

      commit('updateLastReviewed', { cardId, binId });

      // if something is wrong, don't lose the card, send to beginning
      const nextBin = getters.getSortedBins[(currBin.order ?? 0) + 1];

      if (nextBin) {
        commit('moveCard', {
          cardId,
          fromBinId: binId,
          toBinId: nextBin.id,
        });
      }
    },
    registerWrongAnswer({ state, commit, getters }, { cardId, binId }) {
      const card = state.bins[binId].cards.find((c: Card) => c.id === cardId);

      if (!card) {
        return;
      }

      commit('updateLastReviewed', { cardId, binId });
      commit('incrementIncorrectCount', { cardId, binId });

      if (card.incorrectCount < 10) {
        commit('moveCard', {
          cardId,
          fromBinId: binId,
          toBinId: getters.getSortedBins[1].id,
        });
      } else {
        commit('moveCard', {
          cardId,
          fromBinId: binId,
          toBinId: getters.getHardBin.id,
        });
      }
    },
  },
});

const key: InjectionKey<Store<State>> = Symbol();
const useStore = (): Store<State> => baseUseStore(key);

export { key, State, store, useStore };
