import Card from '@/types/Card';

interface Bin {
  id: string;
  name: string;
  cards: Card[];
  waitTimeMs?: number;
  neverShow?: boolean;
  order?: number;
}

export default Bin;
