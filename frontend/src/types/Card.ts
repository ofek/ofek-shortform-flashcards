interface Card {
  id: string;
  word: string;
  definition: string;
  lastReviewed?: Date;
  incorrectCount: number;
}

export default Card;
