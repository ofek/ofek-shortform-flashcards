import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import Admin from '@/views/Admin.vue';
import Review from '@/views/Review.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Review',
    component: Review,
  },
  {
    path: '/admin',
    name: 'Admin',
    component: Admin,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
